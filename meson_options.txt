option('enable-audio-device-selector', type: 'boolean', value: 'true',
  description : 'Enable the audio device selector extension')

option('enable-freebox-remote', type: 'boolean', value: 'false',
  description : 'Enable the Freebox v6 TV player remote extension')
option('freebox-remote-code', type: 'string',
  description : 'Your Freebox v6 TV player remote code')
