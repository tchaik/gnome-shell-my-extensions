// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-

const Lang = imports.lang;
const Mainloop = imports.mainloop;
const Signals = imports.signals;
const Gvc = imports.gi.Gvc;
const St = imports.gi.St;

const Main = imports.ui.main;
const Volume = imports.ui.status.volume;
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;

const MenuItem = new Lang.Class({
    Name: 'AudioDeviceSelector',
    Extends: PopupMenu.PopupSubMenuMenuItem,

    _init: function() {
        this.parent("Audio", true);
        this._devices = []; this._signals = [];
        this._active_device = null;
        this._visibles = [];

        this._control = Volume.getMixerControl();

        this._signals.push(this._control.connect("output-added", Lang.bind(this,
            function (control, id) {
                this._outputAdded(control, id);
            })));

        this._signals.push(this._control.connect("active-output-update", Lang.bind(this,
            function (control, id) {
                this._outputActivated(control, id);
            })));

        this._signals.push(this._control.connect("output-removed", Lang.bind(this,
            function (control, id) {
                this._outputRemoved(control, id);
            })));

        this.menu.addSettingsAction(_("Sound Settings"), 'gnome-sound-panel.desktop');

        let active_sink = this._control.get_default_sink();
        let sink_list = this._control.get_sinks();
        let id = -1;

        for (let i = 0; i < sink_list.length; i++) {
            let sink = sink_list[i];
            let device = null;
            
            if (sink != null)
                device = this._control.lookup_device_from_stream(sink);
            if (device != null) {
                id = this._outputAdded(this._control, device.get_id());
                if (id >= 0 && sink == active_sink)
                    this._active_device = this.devices[id];
            }
        }

        this.actor.show();
    },

    destroy: function() {
        while (this._signals.length > 0) {
            this._control.disconnect(this._signals[0]);
            this._signals.shift();
        }

        this.parent();
    },

    _sync: function() {
        if (this._active_device == null) {
            this.icon.icon_name = 'audio-card-symbolic';
            this.label.text = "No Output";
        } else {
            switch (this._active_device.icon) {
                case 'audio-speakers':
                case 'audio-speakers-bluetooth':
                    this.icon.icon_name = 'audio-speakers-symbolic';
                    break;
                case 'audio-headphones':
                    this.icon.icon_name = 'audio-headphones-symbolic';
                    break;
                case 'audio-headset':
                    this.icon.icon_name = 'audio-headset-symbolic';
                    break;
                default:
                case 'audio-card':
                    this.icon.icon_name = 'audio-card-symbolic';
            }

            this.label.text = this._active_device.description;
        }

        for (let i = 0; i < this._devices.length; i++) {
            if (this._visibles.length <= 1)
                this._devices[i].item.actor.hide();
            else if (this._visibles.indexOf(i) >= 0)
                this._devices[i].item.actor.show();
            else
                this._devices[i].item.actor.hide();
        }
    },

    _outputAdded: function(control, id) {
        let i = -1, device = control.lookup_output_id(id);
        let menu_item, found = false;

        global.log("_outputAdded: " + device.get_id());

        if (device != null) {
            for (let j = 0; j < this._devices.length; j++) {
                if (this._devices[j] == null)
                    continue;
                else if (this._devices[j].id == id) {
                    found = true; i = j;
                    break;
                }
            }

            if (found == false && i < 0) {
                for (let k = 0; k < this._devices.length; k++) {
                    if (this._devices[k].id >= id) {
                        this._devices.splice(k, 0, new Object());
                        found = true; i = k;
                        break;
                    }
                }

                if (found == false && i < 0)
                    i = this._devices.push(new Object()) - 1;

                menu_item = new PopupMenu.PopupMenuItem(device.get_origin());
                menu_item.connect('activate', Lang.bind(this,
                    function (menu_item, event) {
                        control.change_output(device);
                    }));

                this._devices[i].id = id;
                this._devices[i].device = device;
                this._devices[i].description = device.get_description();
                this._devices[i].icon = device.get_icon_name();
                this._devices[i].item = menu_item;

                this.menu.addMenuItem(menu_item, i);
            }

            if (this._visibles.indexOf(i) < 0)
                this._visibles.push(i);
            this._sync();
        }

        return i;
    },

    _outputRemoved: function(control, id) {
        let i = -1, device = control.lookup_output_id(id);

        global.log("_outputRemoved : " + device.get_id());

        if (device != null) {
            for (let j = 0; j < this._devices.length; j++) {
                if (this._devices[j] == null)
                    continue;
                else if (this._devices[j].id == id) {
                    if ((j = this._visibles.indexOf(j)) >= 0)
                        this._visibles.splice(j, 1); i = j;
                    break;
                }
            }

            this._sync();
        }

        return i;
    },

    _outputActivated: function(control, id) {
        let i = -1, device = control.lookup_output_id(id);

        global.log("_outputActivated: " + device.get_id());

        if (device != null) {
            if (this._active_device != null)
                this._active_device.item.setOrnament(PopupMenu.Ornament.NONE);

            for (let j = 0; j < this._devices.length; j++) {
                if (this._devices[j] == null)
                    continue;
                else if (this._devices[j].id == id) {
                    this._active_device = this._devices[j]; i = j;
                    break;
                }
            }

            this._active_device.item.setOrnament(PopupMenu.Ornament.DOT);

            this._sync();
        }

        return i;
    }
});

function init() {
    
}

let _item;

function enable() {
    let panel_menu = Main.panel.statusArea.aggregateMenu;
    let menu_items = panel_menu.menu._getMenuItems();

    _item = new MenuItem();

    for (let i = 0; i < menu_items.length; i++) {
        if (menu_items[i] == panel_menu._power.menu) {
            panel_menu.menu.addMenuItem(_item, i);
            break;
        } else if (menu_items[i] == panel_menu._system.menu) {
            panel_menu.menu.addMenuItem(_item, i);
            break;
        }
    }
}

function disable() {
    _item.destroy();
    _item = null;
}
