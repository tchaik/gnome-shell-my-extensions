// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-

const Clutter = imports.gi.Clutter;
const Lang = imports.lang;
const St = imports.gi.St;
const Shell = imports.gi.Shell;
const Soup = imports.gi.Soup;

const Main = imports.ui.main;
const Panel = imports.ui.panel;
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;
const Signals = imports.signals;

const Me = imports.misc.extensionUtils.getCurrentExtension();

const Config = Me.imports.config

const HTTP_METHOD = "http://hd1.freebox.fr/pub/remote_control?code=%s&key=%s";

let mainIndicator = null;
let httpSession = null;

const RemoteStatusButton = new Lang.Class({
    Name: 'Freemote.RemoteStatusButton',
    Extends: PanelMenu.Button,

    _init: function() {
        this.parent(0.0, "Freemote", false);
        let icon = null, arrow = null;
        let box = null, item = null;
        let table = null, layout = null;

        box = new St.BoxLayout({ style_class: 'panel-status-menu-box' });

        icon = new St.Icon({ style_class: 'system-status-icon', icon_name: 'view-grid-symbolic' });
        box.add_child(icon);

        arrow = new St.Label({ text: '\u25BE', y_expand: false, y_align: Clutter.ActorAlign.CENTER });
        box.add_child(arrow);

        table = new St.Widget({ style_class: 'remote-buttons-table', layout_manager: new Clutter.TableLayout(), reactive: true });

        layout = table.layout_manager;

        item = new St.Button({ style_class: 'remote-flat-button', label: 'AV' });
        item.connect('clicked', Lang.bind(this, function() { this.sendCommand('tv'); }));
        layout.pack(item, 0, 0);

        icon = new St.Icon({ icon_name: 'system-shutdown-symbolic', icon_size: 18 });
        item = new St.Button({ style_class: 'remote-sharp-button', child: icon, button_mask: St.ButtonMask.ONE });
        item.connect('clicked', Lang.bind(this, function() { this.sendCommand('power'); }));
        layout.pack(item, 2, 0);


        item = new St.Button({ style_class: 'remote-flat-button', label: '1' });
        item.connect('clicked', Lang.bind(this, function() { this.sendCommand('1'); }));
        layout.pack(item, 0, 1);

        item = new St.Button({ style_class: 'remote-flat-button', label: '2' });
        item.connect('clicked', Lang.bind(this, function() { this.sendCommand('2'); }));
        layout.pack(item, 1, 1);

        item = new St.Button({ style_class: 'remote-flat-button', label: '3' });
        item.connect('clicked', Lang.bind(this, function() { this.sendCommand('3'); }));
        layout.pack(item, 2, 1);


        item = new St.Button({ style_class: 'remote-flat-button', label: '4' });
        item.connect('clicked', Lang.bind(this, function() { this.sendCommand('4'); }));
        layout.pack(item, 0, 2);

        item = new St.Button({ style_class: 'remote-sharp-button', label: '5' });
        item.connect('clicked', Lang.bind(this, function() { this.sendCommand('5'); }));
        layout.pack(item, 1, 2);

        item = new St.Button({ style_class: 'remote-flat-button', label: '6' });
        item.connect('clicked', Lang.bind(this, function() { this.sendCommand('6'); }));
        layout.pack(item, 2, 2);


        item = new St.Button({ style_class: 'remote-flat-button', label: '7' });
        item.connect('clicked', Lang.bind(this, function() { this.sendCommand('7'); }));
        layout.pack(item, 0, 3);

        item = new St.Button({ style_class: 'remote-flat-button', label: '8' });
        item.connect('clicked', Lang.bind(this, function() { this.sendCommand('8'); }));
        layout.pack(item, 1, 3);

        item = new St.Button({ style_class: 'remote-flat-button', label: '9' });
        item.connect('clicked', Lang.bind(this, function() { this.sendCommand('9'); }));
        layout.pack(item, 2, 3);


        item = new St.Button({ style_class: 'remote-flat-button', label: '0' });
        item.connect('clicked', Lang.bind(this, function() { this.sendCommand('0'); }));
        layout.pack(item, 1, 4);


        icon = new St.Icon({ icon_name: 'edit-undo-symbolic', icon_size: 18 });
        item = new St.Button({ style_class: 'remote-flat-button', child: icon });
        item.connect('clicked', Lang.bind(this, function() { this.sendCommand('red'); }));
        layout.pack(item, 0, 5);

        icon = new St.Icon({ icon_name: 'go-up-symbolic', icon_size: 18 });
        item = new St.Button({ style_class: 'remote-sharp-button', child: icon });
        item.add_style_class_name('remote-sharp-top-button');
        item.connect('clicked', Lang.bind(this, function() { this.sendCommand('up'); }));
        layout.pack(item, 1, 5);

        icon = new St.Icon({ icon_name: 'edit-find-symbolic', icon_size: 18 });
        item = new St.Button({ style_class: 'remote-flat-button', child: icon });
        item.connect('clicked', Lang.bind(this, function() { this.sendCommand('blue'); }));
        layout.pack(item, 2, 5);


        icon = new St.Icon({ icon_name: 'go-previous-symbolic', icon_size: 18 });
        item = new St.Button({ style_class: 'remote-sharp-button', child: icon });
        item.add_style_class_name('remote-sharp-left-button');
        item.connect('clicked', Lang.bind(this, function() { this.sendCommand('left'); }));
        layout.pack(item, 0, 6);

        item = new St.Button({ style_class: 'remote-sharp-button', label: 'OK' });
        item.add_style_class_name('remote-sharp-central-button');
        item.connect('clicked', Lang.bind(this, function() { this.sendCommand('ok'); }));
        layout.pack(item, 1, 6);

        icon = new St.Icon({ icon_name: 'go-next-symbolic', icon_size: 18 });
        item = new St.Button({ style_class: 'remote-sharp-button', child: icon });
        item.add_style_class_name('remote-sharp-right-button');
        item.connect('clicked', Lang.bind(this, function() { this.sendCommand('right'); }));
        layout.pack(item, 2, 6);


        icon = new St.Icon({ icon_name: 'view-list-symbolic', icon_size: 18 });
        item = new St.Button({ style_class: 'remote-flat-button', child: icon });
        item.connect('clicked', Lang.bind(this, function() { this.sendCommand('green'); }));
        layout.pack(item, 0, 7);

        icon = new St.Icon({ icon_name: 'go-down-symbolic', icon_size: 18 });
        item = new St.Button({ style_class: 'remote-sharp-button', child: icon });
        item.add_style_class_name('remote-sharp-bottom-button');
        item.connect('clicked', Lang.bind(this, function() { this.sendCommand('down'); }));
        layout.pack(item, 1, 7);

        icon = new St.Icon({ icon_name: 'dialog-information-symbolic', icon_size: 18 });
        item = new St.Button({ style_class: 'remote-flat-button', child: icon });
        item.connect('clicked', Lang.bind(this, function() { this.sendCommand('yellow'); }));
        layout.pack(item, 2, 7);


        item = new St.Button({ style_class: 'remote-home-label', label: 'free', y_expand: true, y_align: Clutter.ActorAlign.CENTER });
        item.connect('clicked', Lang.bind(this, function() { this.sendCommand('home'); }));
        layout.pack(item, 1, 8);


        icon = new St.Icon({ icon_name: 'list-add-symbolic', icon_size: 18 });
        item = new St.Button({ style_class: 'remote-sharp-button', child: icon });
        item.add_style_class_name('remote-sharp-top-button');
        item.connect('clicked', Lang.bind(this, function() { this.sendCommand('vol_inc'); }));
        layout.pack(item, 0, 9);

        icon = new St.Icon({ icon_name: 'audio-volume-muted-symbolic', icon_size: 18 });
        item = new St.Button({ style_class: 'remote-flat-button', child: icon });
        item.connect('clicked', Lang.bind(this, function() { this.sendCommand('mute'); }));
        layout.pack(item, 1, 9);

        icon = new St.Icon({ icon_name: 'list-add-symbolic', icon_size: 18 });
        item = new St.Button({ style_class: 'remote-sharp-button', child: icon });
        item.add_style_class_name('remote-sharp-top-button');
        item.connect('clicked', Lang.bind(this, function() { this.sendCommand('prgm_inc'); }));
        layout.pack(item, 2, 9);


        item = new St.Label({ style_class: 'remote-sharp-label', text: 'VOL' });
        layout.pack(item, 0, 10);

        item = new St.Label({ style_class: 'remote-sharp-label', text: 'PROG' });
        layout.pack(item, 2, 10);


        icon = new St.Icon({ icon_name: 'list-remove-symbolic', icon_size: 18 });
        item = new St.Button({ style_class: 'remote-sharp-button', child: icon });
        item.add_style_class_name('remote-sharp-bottom-button');
        item.connect('clicked', Lang.bind(this, function() { this.sendCommand('vol_dec'); }));
        layout.pack(item, 0, 11);

        icon = new St.Icon({ icon_name: 'media-record-symbolic', icon_size: 18 });
        item = new St.Button({ style_class: 'remote-flat-button', child: icon });
        item.connect('clicked', Lang.bind(this, function() { this.sendCommand('rec'); }));
        layout.pack(item, 1, 11);

        icon = new St.Icon({ icon_name: 'list-remove-symbolic', icon_size: 18 });
        item = new St.Button({ style_class: 'remote-sharp-button', child: icon });
        item.add_style_class_name('remote-sharp-bottom-button');
        item.connect('clicked', Lang.bind(this, function() { this.sendCommand('prgm_dec'); }));
        layout.pack(item, 2, 11);


        icon = new St.Icon({ icon_name: 'media-seek-backward-symbolic', icon_size: 18 });
        item = new St.Button({ style_class: 'remote-flat-button', child: icon });
        item.connect('clicked', Lang.bind(this, function() { this.sendCommand('bwd'); }));
        layout.pack(item, 0, 12);

        icon = new St.Icon({ icon_name: 'media-playback-start-symbolic', icon_size: 18 });
        item = new St.Button({ style_class: 'remote-flat-button', child: icon });
        item.connect('clicked', Lang.bind(this, function() { this.sendCommand('play'); }));
        layout.pack(item, 1, 12);

        icon = new St.Icon({ icon_name: 'media-seek-forward-symbolic', icon_size: 18 });
        item = new St.Button({ style_class: 'remote-flat-button', child: icon });
        item.connect('clicked', Lang.bind(this, function() { this.sendCommand('fwd'); }));
        layout.pack(item, 2, 12);

        this.menu.box.add(table);

        Main.panel.menuManager.addMenu(this.menu);

        this.actor.add_child(box);
        this.actor.show();
    },

    sendCommand: function(key) {
        let request = null;
        let url = ""

        url = HTTP_METHOD.format(Config.REMOTE_CODE, key);
        request = Soup.Message.new('GET', url);
        httpSession.queue_message(request, Lang.bind(this, function(httpSession, response) {}));
    },

    destroy: function() {
        this.parent();
    },
});

function init() {

}

function enable() {
    mainIndicator = new RemoteStatusButton();
    httpSession = new Soup.Session();

    Main.panel.addToStatusArea('freemote', mainIndicator);

    httpSession.user_agent = "Soup/2.4 (Linux) GnomeShell/3.12";
    httpSession.timeout = 1;
}

function disable() {
    mainIndicator.destroy();
}
